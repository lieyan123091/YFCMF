<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2018 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\AuthRule as AuthRuleModel;
use app\common\controller\Common;

/**
 * 后台基控制器
 * @author rainfer <rainfer520@qq.com>
 */
class Base extends Common
{
    /**
     * 初始化
     */
    public function initialize()
    {
        parent::initialize();
        //检测登录
        if (!$this->checkAdminLogin()) {
            $this->redirect(config('adminpath') . '/Login/index');
        }
        $auth = new AuthRuleModel;
        //检测权限
        $id_curr = $auth->getUrlId();
        if (!$auth->checkAuth($id_curr)) {
            $this->error('没有权限', config('adminpath') . '/Index/index');
        }

        //获取有权限的菜单
        $menus = $auth->getRules();
        $this->assign('menus', $menus);
        $id_curr_arr = $auth->getParents($id_curr);
        $this->assign('id_curr_arr', $id_curr_arr);
    }
}
